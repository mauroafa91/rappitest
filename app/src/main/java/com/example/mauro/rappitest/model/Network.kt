package com.example.mauro.rappitest.model

import java.io.Serializable

class Network: Serializable {
    var name: String = ""
    var id: Int = 0
    var logo_path: String = ""
    var origin_country: String = ""

    constructor()

    constructor(name: String, id: Int, logo_path: String, origin_country: String) {
        this.name = name
        this.id = id
        this.logo_path = logo_path
        this.origin_country = origin_country
    }
}
