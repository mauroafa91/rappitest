package com.example.mauro.rappitest.`interface`

import com.example.mauro.rappitest.model.MovieDetails
import com.example.mauro.rappitest.model.MovieResult
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface IMovie {

    @get:GET("movie/popular/?api_key=d2a74c9956bd870b45fa2a277323dd23")
    val popularMovies: Call<MovieResult>

    @get:GET("movie/top_rated/?api_key=d2a74c9956bd870b45fa2a277323dd23")
    val TopRatedMovies: Call<MovieResult>

    @get:GET("movie/upcoming/?api_key=d2a74c9956bd870b45fa2a277323dd23")
    val UpcomingMovies: Call<MovieResult>

    @GET("movie/{movie_id}?api_key=d2a74c9956bd870b45fa2a277323dd23")
    fun getMovieDetail(@Path("movie_id") movieID: Int): Call<MovieDetails>
}