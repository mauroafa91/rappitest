package com.example.mauro.rappitest.model

import java.io.Serializable

class Season: Serializable {
    var air_date: String = ""
    var episode_count: Int = 0
    var id: Int = 0
    var name: String = ""
    var overview: String = ""
    var poster_path: String = ""
    var season_number: Int = 0

    constructor()

    constructor(air_date: String, episode_count: Int, id: Int, name: String, overview: String, poster_path: String, season_number: Int) {
        this.air_date = air_date
        this.episode_count = episode_count
        this.id = id
        this.name = name
        this.overview = overview
        this.poster_path = poster_path
        this.season_number = season_number
    }

}