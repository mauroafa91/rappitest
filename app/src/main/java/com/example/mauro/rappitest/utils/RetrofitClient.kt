package com.example.mauro.rappitest.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.coroutines.experimental.coroutineContext

object RetrofitClient  {
    private var ourInstance: Retrofit?=null
    var ourHTTPClient: OkHttpClient?=null

    val instance: Retrofit
        get() {
            if(ourInstance == null) {

                ourInstance = Retrofit.Builder()
                        .baseUrl("https://api.themoviedb.org/3/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .client(ourHTTPClient!!)
                        .build()
            }
            return ourInstance!!
        }

}