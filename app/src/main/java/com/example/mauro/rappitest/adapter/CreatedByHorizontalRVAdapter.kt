package com.example.mauro.rappitest.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.mauro.rappitest.R
import com.example.mauro.rappitest.model.CreatedBy
import com.example.mauro.rappitest.model.ProductionCompany
import kotlinx.android.synthetic.main.horizontal_list_content.view.*

class CreatedByHorizontalRVAdapter(private var alProductionCompany: ArrayList<CreatedBy>) :
        RecyclerView.Adapter<CreatedByHorizontalRVAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.horizontal_list_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val item = alProductionCompany[position]
        //Set movie name
        holder.productionCompanyName.text = item.name

        Glide.with(holder.idView.context)
                .load(String.format("https://image.tmdb.org/t/p/w500%s", item.profile_path))
                .into(holder.idView)
        with(holder.itemView) {
            tag = item
        }
    }

    override fun getItemCount() = alProductionCompany.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val idView: ImageView = view.iv_production_company
        val productionCompanyName: TextView = view.tv_production_company_name
    }
}