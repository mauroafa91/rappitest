package com.example.mauro.rappitest.model

import java.io.Serializable

class SpokenLanguage : Serializable {
    var iso_639_1: String = ""
    var name: String = ""

    constructor()

    constructor(iso_639_1: String, name: String) {
        this.iso_639_1 = iso_639_1
        this.name = name
    }
}