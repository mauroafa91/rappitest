package com.example.mauro.rappitest.model

import java.io.Serializable

class ProductionCountry : Serializable {
    var iso_3166_1: String = ""
    var name: String = ""

    constructor()

    constructor(iso_3166_1: String, name: String) {
        this.iso_3166_1 = iso_3166_1
        this.name = name
    }
}