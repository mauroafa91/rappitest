package com.example.mauro.rappitest.model

import java.io.Serializable

class LastEpisodeToAir: Serializable {
    var air_date: String = ""
    var episode_number: Int = 0
    var id: Int = 0
    var name: String = ""
    var overview: String = ""
    var production_code: String = ""
    var season_number: Int = 0
    var show_id: Int = 0
    var still_path: String = ""
    var vote_average: Double = 0.0
    var vote_count: Int = 0

    constructor()

    constructor(air_date: String, episode_number: Int, id: Int, name: String, overview: String, production_code: String, season_number: Int, show_id: Int, still_path: String, vote_average: Double, vote_count: Int) {
        this.air_date = air_date
        this.episode_number = episode_number
        this.id = id
        this.name = name
        this.overview = overview
        this.production_code = production_code
        this.season_number = season_number
        this.show_id = show_id
        this.still_path = still_path
        this.vote_average = vote_average
        this.vote_count = vote_count
    }


}