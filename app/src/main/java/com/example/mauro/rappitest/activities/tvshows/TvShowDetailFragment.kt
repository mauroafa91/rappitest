package com.example.mauro.rappitest.activities.tvshows

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.example.mauro.rappitest.R
import com.example.mauro.rappitest.adapter.CreatedByHorizontalRVAdapter
import com.example.mauro.rappitest.adapter.NetworkHorizontalRVAdapter
import com.example.mauro.rappitest.adapter.ProductionCompaniesHorizontalRVAdapter
import com.example.mauro.rappitest.model.Genres
import com.example.mauro.rappitest.model.TVShowDetails
import com.example.mauro.rappitest.model.TvShow
import kotlinx.android.synthetic.main.tvshow_detail.view.*
import kotlinx.android.synthetic.main.activity_popular_detail.*

/**
 * A fragment representing a single TvShow detail screen.
 * This fragment is either contained in a [TvShowActivity]
 * in two-pane mode (on tablets) or a [TvShowDetailActivity]
 * on handsets.
 */
class TvShowDetailFragment : Fragment() {

    /**
     * The dummy content this fragment is presenting.
     */
    private var item: TVShowDetails? = null
    private var genreObject: Genres = Genres()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            if (it.containsKey(ARG_ITEM_ID)) {
                // Load the dummy content specified by the fragment
                // arguments. In a real-world scenario, use a Loader
                // to load content from a content provider.
                item = it.getSerializable(ARG_ITEM_ID) as TVShowDetails
            }
            if (it.containsKey(ARG_GENRES)) {
                genreObject = it.getSerializable(ARG_GENRES) as Genres
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.tvshow_detail, container, false)

        // Show the dummy content as text in a TextView.
        item.let {

            activity?.toolbar_layout!!.title = item!!.name
            Glide.with(activity?.iv_movie_backdrop!!.context)
                    .load(String.format("https://image.tmdb.org/t/p/w500%s", item!!.backdrop_path))
                    .into(activity?.iv_movie_backdrop!!)
            var formattedText = ""

            rootView.rb_movie.rating = it!!.vote_average.toFloat()
            rootView.tv_rating.text = String.format("Vote count: %s", it.vote_count.toString())
            rootView.tv_in_production.text = if(it.in_production) "Yes" else "No"
            rootView.tv_homepage.text = it.homepage //Make clickable
            rootView.tv_status.text = it.status
            rootView.tv_scrypted.text = it.type

            it.episode_run_time.forEach {
                formattedText += it.toString() + ", "
            }
            rootView.tv_episode_runtime.text = formattedText.removeSuffix(", ")
            formattedText = ""

            rootView.tv_first_air_date.text = it.first_air_date
            rootView.tv_last_air_date.text = it.last_air_date

            rootView.tv_overview.text = it.overview
            rootView.tv_original_language.text = it.original_language
            rootView.tv_original_title.text = it.original_name

            it.languages.forEach {
                formattedText += "$it, "
            }
            rootView.tv_spoken_languages.text = formattedText.removeSuffix(", ")
            formattedText = ""

            it.genres.forEach {
                formattedText += "${it.name}, "
            }
            rootView.tv_genres.text = formattedText.removeSuffix(", ")
            formattedText = ""

            rootView.rv_production_companies.layoutManager = LinearLayoutManager(context, LinearLayout.HORIZONTAL, false)
            rootView.rv_production_companies.adapter = ProductionCompaniesHorizontalRVAdapter(it.production_companies)

            rootView.rv_created_by.layoutManager = LinearLayoutManager(context, LinearLayout.HORIZONTAL, false)
            rootView.rv_created_by.adapter = CreatedByHorizontalRVAdapter(it.created_by)

            rootView.rv_networks.layoutManager = LinearLayoutManager(context, LinearLayout.HORIZONTAL, false)
            rootView.rv_networks.adapter = NetworkHorizontalRVAdapter(it.networks)

            rootView.tv_number_episodes.text = String.format("Number of episodes: %s", it.number_of_episodes.toString())
            rootView.tv_number_seasons.text = String.format("Number of seasons: %s", it.number_of_seasons.toString())
        }

        return rootView
    }

    companion object {
        /**
         * The fragment argument representing the item ID that this fragment
         * represents.
         */
        const val ARG_ITEM_ID = "item_id"
        const val ARG_GENRES = "genreObject"
    }
}
