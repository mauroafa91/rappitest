package com.example.mauro.rappitest.model

import java.io.Serializable

class CreatedBy: Serializable {
    var id: Int = 0
    var credit_id: String = ""
    var name: String = ""
    var gender: Int = 0
    var profile_path: String = ""

    constructor()

    constructor(id: Int, credit_id: String, name: String, gender: Int, profile_path: String) {
        this.id = id
        this.credit_id = credit_id
        this.name = name
        this.gender = gender
        this.profile_path = profile_path
    }
}