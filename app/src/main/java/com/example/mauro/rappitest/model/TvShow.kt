package com.example.mauro.rappitest.model

import java.io.Serializable

class TvShow : Serializable {
    var poster_path: String = ""
    var popularity: Double = 0.0
    var id: Int = 0
    var backdrop_path: String = ""
    var vote_average: Double = 0.0
    var overview: String = ""
    var first_air_date: String = ""
    var origin_country: ArrayList<String> = ArrayList()
    var genre_ids: ArrayList<Int> = ArrayList()
    var original_language: String = ""
    var vote_count: Int = 0
    var name: String = ""
    var original_name: String = ""

    constructor()

    constructor(poster_path: String, popularity: Double, id: Int, backdrop_path: String, vote_average: Double, overview: String, first_air_date: String, origin_country: ArrayList<String>, genre_ids: ArrayList<Int>, original_language: String, vote_count: Int, name: String, original_name: String) {
        this.poster_path = poster_path
        this.popularity = popularity
        this.id = id
        this.backdrop_path = backdrop_path
        this.vote_average = vote_average
        this.overview = overview
        this.first_air_date = first_air_date
        this.origin_country = origin_country
        this.genre_ids = genre_ids
        this.original_language = original_language
        this.vote_count = vote_count
        this.name = name
        this.original_name = original_name
    }
}