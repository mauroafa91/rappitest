package com.example.mauro.rappitest.model

import java.io.Serializable

class ProductionCompany : Serializable {
    var id: Int = 0
    var logo_path: String = ""
    var name: String = ""
    var origin_country: String = ""

    constructor()

    constructor(id: Int, logo_path: String, name: String, origin_country: String) {
        this.id = id
        this.logo_path = logo_path
        this.name = name
        this.origin_country = origin_country
    }


}