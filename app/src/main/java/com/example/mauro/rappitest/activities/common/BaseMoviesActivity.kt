package com.example.mauro.rappitest.activities.common

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.ViewGroup
import android.widget.SearchView
import com.example.mauro.rappitest.R
import com.example.mauro.rappitest.`interface`.IMovie
import com.example.mauro.rappitest.`interface`.IUtils
import com.example.mauro.rappitest.activities.movies.PopularMoviesActivity
import com.example.mauro.rappitest.activities.movies.TopRatedMoviesActivity
import com.example.mauro.rappitest.activities.movies.UpcomingMoviesActivity
import com.example.mauro.rappitest.activities.tvshows.PopularTvShowActivity
import com.example.mauro.rappitest.adapter.MoviesRecyclerViewAdapter
import com.example.mauro.rappitest.model.Genres
import com.example.mauro.rappitest.model.MovieResult
import com.example.mauro.rappitest.utils.RetrofitClient
import kotlinx.android.synthetic.main.activity_base.*
import kotlinx.android.synthetic.main.activity_content.*
import kotlinx.android.synthetic.main.popular_list.*
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback

open class BaseMoviesActivity : AppCompatActivity() , SearchView.OnQueryTextListener {

    private val TAG = "BaseMoviesActivity"
    var mMoviesAdapter: MoviesRecyclerViewAdapter? = null
    private var movieGenre: Genres? = null
    internal lateinit var movieAPI: IMovie
    internal lateinit var utilsAPI: IUtils

    override fun onQueryTextSubmit(query: String?): Boolean {
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if (newText != null) {
            mMoviesAdapter!!.filterMovie(newText)
        }
        return true
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_popular -> {
                startActivity(Intent(this, PopularMoviesActivity::class.java))
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_top_rated -> {
                startActivity(Intent(this, TopRatedMoviesActivity::class.java))
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_upcoming -> {
                startActivity(Intent(this, UpcomingMoviesActivity::class.java))
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when(item!!.itemId) {
            R.id.navigation_to_series -> {
                startActivity(Intent(this, PopularTvShowActivity::class.java))
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menu!!.findItem(R.id.navigation_to_movies).isVisible = false
        menu.findItem(R.id.navigation_to_series).isVisible = true
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.top_navigation, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val mItem = menu!!.findItem(R.id.navigation_search) as MenuItem
        val searchView: SearchView = mItem.actionView as SearchView

        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setIconifiedByDefault(true)
        searchView.setOnQueryTextListener(this)
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.setContentView(R.layout.activity_base)
        setSupportActionBar(toolbar)

        val cacheSize: Long = 10*1024*1024
        val cache = Cache(cacheDir, cacheSize)

        RetrofitClient.ourHTTPClient = OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor { chain ->
                    var request = chain.request()
                    request = if (hasNetwork(applicationContext)!!)
                        request.newBuilder().header("Cache-Control", "public, max-age=" + 5).build()
                    else
                        request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build()
                    chain.proceed(request)
                }
                .build()
        val retroFit = RetrofitClient.instance
        movieAPI = retroFit.create(IMovie::class.java)
        utilsAPI = retroFit.create(IUtils::class.java)

        fillGenres()

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    override fun setContentView(layoutResID: Int) {
        if (frame != null) {
            val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val lp = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT)
            val stubView = inflater.inflate(layoutResID, frame, false)
            frame.addView(stubView, lp)
        }
    }

    fun setCheckedItem(index: Int) {
        navigation.menu.getItem(index).setChecked(true)
    }

    fun hasNetwork(context: Context): Boolean? {
        var isConnected: Boolean? = false // Initial Value
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        if (activeNetwork != null && activeNetwork.isConnected)
            isConnected = true
        return isConnected
    }


    fun FillMovieData(APIData: MovieResult, twoPane: Boolean, selectionType: Int) {
        popular_list.layoutManager = GridLayoutManager(applicationContext, 2, GridLayoutManager.VERTICAL, false)
        mMoviesAdapter = MoviesRecyclerViewAdapter(this, APIData, twoPane, selectionType, movieGenre!!.genres)
        popular_list.adapter = mMoviesAdapter
    }

    fun fillGenres() {
        val allGenres = utilsAPI.movieGenres
        allGenres.enqueue(object: Callback<Genres> {
            override fun onFailure(call: Call<Genres>, t: Throwable) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onResponse(call: Call<Genres>, response: retrofit2.Response<Genres>) {
                if(response.code() != 504)
                {
                    movieGenre = response.body()
                }
            }
        })
    }
}
