package com.example.mauro.rappitest.activities.tvshows

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import com.example.mauro.rappitest.R
import com.example.mauro.rappitest.activities.common.BaseTvShowsActivity
import com.example.mauro.rappitest.adapter.TvShowRecyclerViewAdapter
import com.example.mauro.rappitest.model.TVShowResult
import kotlinx.android.synthetic.main.tvshow_list.*
import retrofit2.Call
import retrofit2.Callback

/**
 * An activity representing a list of Pings. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a [TvShowDetailActivity] representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
class TopRatedTvShowActivity : BaseTvShowsActivity() {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private var twoPane: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tvshow_list)
        setCheckedItem(1)

        if (tvshow_detail_container != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            twoPane = true
        }

        val tvShowTopRated = tvShowAPI.topRatedTVShows
        tvShowTopRated.enqueue(object: Callback<TVShowResult> {
            override fun onFailure(call: Call<TVShowResult>, t: Throwable) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onResponse(call: Call<TVShowResult>, response: retrofit2.Response<TVShowResult>) {
                if(response.code() != 504)
                {
                    val TVShowResult = response.body()

                    if(TVShowResult != null) {
                        FillTVShowData(TVShowResult, twoPane, 2)
                    }
                }
            }
        })
    }
}
