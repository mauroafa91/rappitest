package com.example.mauro.rappitest.model

import java.io.Serializable

class TVShowDetails: Serializable {
    var backdrop_path: String = ""
    var created_by: ArrayList<CreatedBy> = ArrayList()
    var episode_run_time: ArrayList<Int> = ArrayList()
    var first_air_date: String = ""
    var genres: ArrayList<Genre> = ArrayList()
    var homepage: String = ""
    var id: Int = 0
    var in_production: Boolean = false
    var languages: ArrayList<String> = ArrayList()
    var last_air_date: String = ""
    var last_episode_to_air: LastEpisodeToAir = LastEpisodeToAir()
    var name: String = ""
    var next_episode_to_air: Any = Any()
    var networks: ArrayList<Network> = ArrayList()
    var number_of_episodes: Int = 0
    var number_of_seasons: Int = 0
    var origin_country: ArrayList<String> = ArrayList()
    var original_language: String = ""
    var original_name: String = ""
    var overview: String = ""
    var popularity: Double = 0.0
    var poster_path: String = ""
    var production_companies: ArrayList<ProductionCompany> = ArrayList()
    var seasons: ArrayList<Season> = ArrayList()
    var status: String = ""
    var type: String = ""
    var vote_average: Double = 0.0
    var vote_count: Int = 0

    constructor()

    constructor(backdrop_path: String, created_by: ArrayList<CreatedBy>, episode_run_time: ArrayList<Int>, first_air_date: String, genres: ArrayList<Genre>, homepage: String, id: Int, in_production: Boolean, languages: ArrayList<String>, last_air_date: String, last_episode_to_air: LastEpisodeToAir, name: String, next_episode_to_air: Any, networks: ArrayList<Network>, number_of_episodes: Int, number_of_seasons: Int, origin_country: ArrayList<String>, original_language: String, original_name: String, overview: String, popularity: Double, poster_path: String, production_companies: ArrayList<ProductionCompany>, seasons: ArrayList<Season>, status: String, type: String, vote_average: Double, vote_count: Int) {
        this.backdrop_path = backdrop_path
        this.created_by = created_by
        this.episode_run_time = episode_run_time
        this.first_air_date = first_air_date
        this.genres = genres
        this.homepage = homepage
        this.id = id
        this.in_production = in_production
        this.languages = languages
        this.last_air_date = last_air_date
        this.last_episode_to_air = last_episode_to_air
        this.name = name
        this.next_episode_to_air = next_episode_to_air
        this.networks = networks
        this.number_of_episodes = number_of_episodes
        this.number_of_seasons = number_of_seasons
        this.origin_country = origin_country
        this.original_language = original_language
        this.original_name = original_name
        this.overview = overview
        this.popularity = popularity
        this.poster_path = poster_path
        this.production_companies = production_companies
        this.seasons = seasons
        this.status = status
        this.type = type
        this.vote_average = vote_average
        this.vote_count = vote_count
    }
}