package com.example.mauro.rappitest.`interface`

import com.example.mauro.rappitest.model.Genres
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET

interface IUtils {
    @get:GET("genre/movie/list?api_key=d2a74c9956bd870b45fa2a277323dd23")
    val movieGenres: Call<Genres>

    @get:GET("genre/tv/list?api_key=d2a74c9956bd870b45fa2a277323dd23")
    val tvShowGenres: Call<Genres>
}