package com.example.mauro.rappitest.adapter

import android.content.Intent
import android.os.Bundle
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.mauro.rappitest.R
import com.example.mauro.rappitest.activities.common.BaseMoviesActivity
import com.example.mauro.rappitest.activities.movies.*
import com.example.mauro.rappitest.model.Genre
import com.example.mauro.rappitest.model.Movie
import com.example.mauro.rappitest.model.MovieResult
import kotlinx.android.synthetic.main.popular_list_content.view.*
import java.lang.NullPointerException

class MoviesRecyclerViewAdapter(private val parentActivity: BaseMoviesActivity,
                                private val values: MovieResult,
                                private val twoPane: Boolean,
                                private val selectionType: Int,
                                private val moviesGenres: ArrayList<Genre>) :
        RecyclerView.Adapter<MoviesRecyclerViewAdapter.ViewHolder>() {

    private val onClickListener: View.OnClickListener
    private var movieList: ArrayList<Movie> = ArrayList()
    private var filteredMovies: ArrayList<Movie> = ArrayList()
    private var bFiltering = false
    private var bFirstUse = true

    init {
        movieList = ArrayList(values.results)
        onClickListener = View.OnClickListener { v ->
            val item = v.tag as Movie
            if (twoPane) {
                val fragment = MoviesDetailFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(MoviesDetailFragment.ARG_ITEM_ID, item.id)
                    }
                }
                parentActivity.supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.popular_detail_container, fragment)
                        .commit()
            } else {
                val intent = Intent(v.context, MoviesDetailActivity::class.java).apply {
                    putExtra(MoviesDetailFragment.ARG_ITEM_ID, item.id)
                    putExtra(MoviesDetailActivity.ARG_SELECTION_TYPE, selectionType)
                }
                v.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.popular_list_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var item: Movie = Movie()
        if(bFiltering) {
            item = filteredMovies[position]
        }
        else {
            item = movieList[position]
        }
        //Set movie name
        holder.tvMovieName.text = item.title

        Glide.with(holder.idView.context)
                .load(String.format("https://image.tmdb.org/t/p/w500%s", item.poster_path))
                .into(holder.idView)
        with(holder.itemView) {
            tag = item
            setOnClickListener(onClickListener)
        }
    }

    fun filterMovie(query: String) {
        bFiltering = true

        if(query == "") {
            filteredMovies.clear()
            DiffUtil.calculateDiff(PhotoRowDiffCallback(filteredMovies, movieList), false).dispatchUpdatesTo(this)
            bFiltering = false
        }
        else {
            val newMovies = movieList.filter { Movie ->
                val a = (moviesGenres.find { it -> it.name.toLowerCase().contains(query.toLowerCase()) })
                if(a != null) {
                    Movie.genre_ids.contains(a.id)
                }
                else {
                    false
                }

            } as ArrayList<Movie>

            if (newMovies.size == movieList.size && !bFirstUse ) {
                DiffUtil.calculateDiff(PhotoRowDiffCallback(newMovies, filteredMovies), false).dispatchUpdatesTo(this)
            }
            else {
                DiffUtil.calculateDiff(PhotoRowDiffCallback(newMovies, movieList), false).dispatchUpdatesTo(this)
                bFirstUse = false
            }

            filteredMovies = newMovies
        }
    }

    override fun getItemCount() = if(bFiltering) filteredMovies.size else movieList.size

    inner class PhotoRowDiffCallback(private val newRows : List<Movie>, private val oldRows : List<Movie>) : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldRow = oldRows[oldItemPosition]
            val newRow = newRows[newItemPosition]
            return oldRow == newRow
        }

        override fun getOldListSize(): Int = oldRows.size

        override fun getNewListSize(): Int = newRows.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldRow = oldRows[oldItemPosition]
            val newRow = newRows[newItemPosition]
            return oldRow == newRow
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val idView: ImageView = view.iv_movie
        val tvMovieName: TextView = view.tv_movie_name
    }
}