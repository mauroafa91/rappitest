package com.example.mauro.rappitest.model

import java.io.Serializable

class MovieDetails: Serializable {

    var adult: Boolean = false
    var backdrop_path: String = ""
    var belongs_to_collection: Any? = null
    var budget: Int = 0
    var genres: List<Genre> = ArrayList()
    var homepage: String = ""
    var id: Int = 0
    var imdb_id: String = ""
    var original_language: String = ""
    var original_title: String = ""
    var overview: String = ""
    var popularity: Double = 0.0
    var poster_path: String = ""
    var production_companies: ArrayList<ProductionCompany> = ArrayList()
    var production_countries: ArrayList<ProductionCountry> = ArrayList()
    var release_date: String = ""
    var revenue: Int = 0
    var runtime: Int = 0
    var spoken_languages: ArrayList<SpokenLanguage> = ArrayList()
    var status: String = ""
    var tagline: String = ""
    var title: String = ""
    var video: Boolean = false
    var vote_average: Double = 0.0
    var vote_count: Int = 0

    constructor()

    constructor(adult: Boolean, backdrop_path: String, belongs_to_collection: Any?, budget: Int, genres: List<Genre>, homepage: String, id: Int, imdb_id: String, original_language: String, original_title: String, overview: String, popularity: Double, poster_path: String, production_companies: ArrayList<ProductionCompany>, production_countries: ArrayList<ProductionCountry>, release_date: String, revenue: Int, runtime: Int, spoken_languages: ArrayList<SpokenLanguage>, status: String, tagline: String, title: String, video: Boolean, vote_average: Double, vote_count: Int) {
        this.adult = adult
        this.backdrop_path = backdrop_path
        this.belongs_to_collection = belongs_to_collection
        this.budget = budget
        this.genres = genres
        this.homepage = homepage
        this.id = id
        this.imdb_id = imdb_id
        this.original_language = original_language
        this.original_title = original_title
        this.overview = overview
        this.popularity = popularity
        this.poster_path = poster_path
        this.production_companies = production_companies
        this.production_countries = production_countries
        this.release_date = release_date
        this.revenue = revenue
        this.runtime = runtime
        this.spoken_languages = spoken_languages
        this.status = status
        this.tagline = tagline
        this.title = title
        this.video = video
        this.vote_average = vote_average
        this.vote_count = vote_count
    }
}