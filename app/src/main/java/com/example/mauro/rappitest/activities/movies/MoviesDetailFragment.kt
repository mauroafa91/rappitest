package com.example.mauro.rappitest.activities.movies

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.example.mauro.rappitest.R
import com.example.mauro.rappitest.adapter.ProductionCompaniesHorizontalRVAdapter
import com.example.mauro.rappitest.model.Genres
import com.example.mauro.rappitest.model.MovieDetails
import kotlinx.android.synthetic.main.activity_popular_detail.*
import kotlinx.android.synthetic.main.movies_detail.view.*

/**
 * A fragment representing a single Popular detail screen.
 * This fragment is either contained in a [PopularMoviesActivity]
 * in two-pane mode (on tablets) or a [MoviesDetailActivity]
 * on handsets.
 */
class MoviesDetailFragment : Fragment() {

    /**
     * The dummy content this fragment is presenting.
     */
    private var item: MovieDetails? = null
    private var genreObject: Genres = Genres()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            if (it.containsKey(ARG_ITEM_ID)) {
                // Load the dummy content specified by the fragment
                // arguments. In a real-world scenario, use a Loader
                // to load content from a content provider.
                item = it.getSerializable(ARG_ITEM_ID) as MovieDetails
            }
            if (it.containsKey(ARG_GENRES)) {
                genreObject = it.getSerializable(ARG_GENRES) as Genres
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.movies_detail, container, false)

        // Show the dummy content as text in a TextView.
        item?.let {

            activity?.toolbar_layout?.title = item?.title
            Glide.with(activity?.iv_movie_backdrop!!.context)
                    .load(String.format("https://image.tmdb.org/t/p/w500%s", item!!.backdrop_path))
                    .into(activity?.iv_movie_backdrop!!)

            var formattedText = ""

            //Set rating bar information
            rootView.rb_movie.rating = it.vote_average.toFloat()
            rootView.tv_rating.text = String.format("Vote count: %s", it.vote_count.toString())
            rootView.tv_tagline.text = it.tagline
            rootView.tv_status.text = it.status
            rootView.tv_revenue.text = it.revenue.toString()
            rootView.tv_runtime.text = it.runtime.toString() //Maybe convert this to hours?
            rootView.tv_website.text = it.homepage //Make clickable
            rootView.tv_budget.text = it.budget.toString()




            rootView.tv_overview.text = it.overview

            rootView.tv_original_language.text = it.original_language

            rootView.tv_original_title.text = it.original_title

            rootView.tv_release_date.text = it.release_date

            rootView.tv_genres.text = ""


            it.genres.forEach { it2 ->
                formattedText += (it2.name + ", ")
            }

            rootView.tv_genres.text = formattedText.removeSuffix(", ")

            it.production_countries.forEach { it2 ->
                formattedText += it2.name + ", "
            }

            rootView.tv_production_countries.text = formattedText.removeSuffix(", ")

            rootView.rv_production_companies.layoutManager = LinearLayoutManager(context, LinearLayout.HORIZONTAL, false)
            rootView.rv_production_companies.adapter = ProductionCompaniesHorizontalRVAdapter(it.production_companies)
        }

        return rootView
    }

    companion object {
        /**
         * The fragment argument representing the item ID that this fragment
         * represents.
         */
        const val ARG_ITEM_ID = "item_id"
        const val ARG_GENRES = "genreObject"
    }
}
