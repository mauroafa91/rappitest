package com.example.mauro.rappitest.model

import com.example.mauro.rappitest.model.Movie
import java.io.Serializable

class MovieResult: Serializable {

    var page: Int = 0
    var results: ArrayList<Movie> = ArrayList()
    var total_results: Int = 0
    var total_pages: Int = 0

    constructor()

    constructor(page: Int, results: ArrayList<Movie>, total_results: Int, total_pages: Int) {
        this.page = page
        this.results = results
        this.total_results = total_results
        this.total_pages = total_pages
    }
}