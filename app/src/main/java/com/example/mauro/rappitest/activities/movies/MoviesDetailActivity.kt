package com.example.mauro.rappitest.activities.movies

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.example.mauro.rappitest.R
import com.example.mauro.rappitest.`interface`.IMovie
import com.example.mauro.rappitest.model.Genres
import com.example.mauro.rappitest.model.MovieDetails
import com.example.mauro.rappitest.utils.RetrofitClient
import kotlinx.android.synthetic.main.activity_popular_detail.*
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Call
import retrofit2.Callback
import java.nio.channels.SelectableChannel

/**
 * An activity representing a single Popular detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a [PopularMoviesActivity].
 */
class MoviesDetailActivity : AppCompatActivity() {

    private var genres: Genres = Genres()
    private var selectionType: Int = 0
    lateinit var movieAPI: IMovie

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_popular_detail)
        setSupportActionBar(detail_toolbar)

        // Show the Up button in the action bar.
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {

            val cacheSize: Long = 10*1024*1024
            val cache = Cache(cacheDir, cacheSize)

            RetrofitClient.ourHTTPClient = OkHttpClient.Builder()
                    .cache(cache)
                    .addInterceptor { chain ->
                        var request = chain.request()
                        request = if (hasNetwork(applicationContext)!!)
                            request.newBuilder().header("Cache-Control", "public, max-age=" + 5).build()
                        else
                            request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build()
                        chain.proceed(request)
                    }
                    .build()

            val retroFit = RetrofitClient.instance
            movieAPI = retroFit.create(IMovie::class.java)
            val movieDetails = movieAPI.getMovieDetail(intent.getIntExtra(MoviesDetailFragment.ARG_ITEM_ID, 0))

            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            movieDetails.enqueue(object: Callback<MovieDetails> {
                override fun onFailure(call: Call<MovieDetails>, t: Throwable) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onResponse(call: Call<MovieDetails>, response: retrofit2.Response<MovieDetails>) {
                    if(response.code() != 504)
                    {
                        val movieData = response.body()
                        val fragment = MoviesDetailFragment().apply {
                            arguments = Bundle().apply {
                                putSerializable(MoviesDetailFragment.ARG_ITEM_ID,
                                        movieData)
                            }
                        }

                        supportFragmentManager.beginTransaction()
                                .add(R.id.popular_detail_container, fragment)
                                .commit()
                    }
                }
            })
        }
        if(intent.extras != null) {
            selectionType = intent.getIntExtra(ARG_SELECTION_TYPE, 0)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) =
            when (item.itemId) {
                android.R.id.home -> {
                    // This ID represents the Home or Up button. In the case of this
                    // activity, the Up button is shown. For
                    // more details, see the Navigation pattern on Android Design:
                    //
                    // http://developer.android.com/design/patterns/navigation.html#up-vs-back
                    when(selectionType) {
                        1 -> {
                            navigateUpTo(Intent(this, PopularMoviesActivity::class.java))
                        }
                        2 -> {
                            navigateUpTo(Intent(this, TopRatedMoviesActivity::class.java))
                        }
                        3 -> {
                            navigateUpTo(Intent(this, UpcomingMoviesActivity::class.java))
                        }
                    }
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    companion object {
        /**
         * The fragment argument representing the item ID that this fragment
         * represents.
         */
        const val ARG_SELECTION_TYPE = "selection_type"
    }

    fun hasNetwork(context: Context): Boolean? {
        var isConnected: Boolean? = false // Initial Value
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        if (activeNetwork != null && activeNetwork.isConnected)
            isConnected = true
        return isConnected
    }
}
