package com.example.mauro.rappitest.`interface`

import com.example.mauro.rappitest.model.TVShowDetails
import com.example.mauro.rappitest.model.TVShowResult
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ITVShows {

    @get:GET("tv/popular/?api_key=d2a74c9956bd870b45fa2a277323dd23")
    val popularTVShows: Call<TVShowResult>

    @get:GET("tv/top_rated/?api_key=d2a74c9956bd870b45fa2a277323dd23")
    val topRatedTVShows: Call<TVShowResult>

    @GET("tv/{tv_id}?api_key=d2a74c9956bd870b45fa2a277323dd23")
    fun getTVShowDetail(@Path("tv_id") tvID: Int): Call<TVShowDetails>

}