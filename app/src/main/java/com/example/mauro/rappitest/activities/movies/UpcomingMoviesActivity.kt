package com.example.mauro.rappitest.activities.movies

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import com.example.mauro.rappitest.R
import com.example.mauro.rappitest.activities.common.BaseMoviesActivity
import com.example.mauro.rappitest.adapter.MoviesRecyclerViewAdapter
import com.example.mauro.rappitest.model.MovieResult
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_upcomingmovies_list.*
import kotlinx.android.synthetic.main.popular_list.*
import retrofit2.Call
import retrofit2.Callback

/**
 * An activity representing a list of Pings. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a [UpcomingMoviesDetailActivity] representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
class UpcomingMoviesActivity : BaseMoviesActivity() {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private var twoPane: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_popular_list)
        setCheckedItem(2)

        setSupportActionBar(toolbar)
        toolbar.title = title

        if (popular_detail_container != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            twoPane = true
        }

        val topRatedMovies = movieAPI.UpcomingMovies
        topRatedMovies.enqueue(object: Callback<MovieResult> {
            override fun onFailure(call: Call<MovieResult>, t: Throwable) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onResponse(call: Call<MovieResult>, response: retrofit2.Response<MovieResult>) {
                if(response.code() != 504)
                {
                    val MovieData = response.body()

                    if(MovieData != null) {
                        FillMovieData(MovieData, twoPane, 3)
                    }
                }
            }
        })
    }
}
