package com.example.mauro.rappitest.adapter

import android.content.Intent
import android.media.tv.TvContract
import android.os.Bundle
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.mauro.rappitest.R
import com.example.mauro.rappitest.activities.common.BaseTvShowsActivity
import com.example.mauro.rappitest.activities.tvshows.TvShowDetailActivity
import com.example.mauro.rappitest.activities.tvshows.TvShowDetailFragment
import com.example.mauro.rappitest.activities.tvshows.PopularTvShowActivity
import com.example.mauro.rappitest.model.Genre
import com.example.mauro.rappitest.model.TVShowResult
import com.example.mauro.rappitest.model.TvShow
import kotlinx.android.synthetic.main.popular_list_content.view.*

class TvShowRecyclerViewAdapter(private val parentTvShowActivity: BaseTvShowsActivity,
                                private val values: TVShowResult,
                                private val twoPane: Boolean,
                                private val selectionType: Int,
                                private val moviesGenres: ArrayList<Genre>) :
        RecyclerView.Adapter<TvShowRecyclerViewAdapter.ViewHolder>() {

    private val onClickListener: View.OnClickListener
    private var TVShowsList: ArrayList<TvShow> = ArrayList()
    private var filteredTVShows: ArrayList<TvShow> = ArrayList()
    private var bFiltering = false
    private var bFirstUse = true

    init {
        TVShowsList = ArrayList(values.results)
        onClickListener = View.OnClickListener { v ->
            val item = v.tag as TvShow
            if (twoPane) {
                val fragment = TvShowDetailFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(TvShowDetailFragment.ARG_ITEM_ID, item.id)
                    }
                }
                parentTvShowActivity.supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.popular_detail_container, fragment)
                        .commit()
            } else {
                val intent = Intent(v.context, TvShowDetailActivity::class.java).apply {
                    putExtra(TvShowDetailFragment.ARG_ITEM_ID, item.id)
                    putExtra(TvShowDetailActivity.ARG_SELECTION_TYPE, selectionType)
                }
                v.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.popular_list_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var item = TvShow()
        if(bFiltering) {
            item = filteredTVShows[position]
        }
        else {
            item = TVShowsList[position]
        }

        //Set movie name
        holder.tvMovieName.text = item.name

        Glide.with(holder.idView.context)
                .load(String.format("https://image.tmdb.org/t/p/w500%s", item.poster_path))
                .into(holder.idView)
        with(holder.itemView) {
            tag = item
            setOnClickListener(onClickListener)
        }
    }

    fun filterTvShows(query: String) {
        bFiltering = true

        if(query == "") {
            filteredTVShows.clear()
            DiffUtil.calculateDiff(PhotoRowDiffCallback(filteredTVShows, TVShowsList), false).dispatchUpdatesTo(this)
            bFiltering = false
        }
        else {
            val newTvShow = TVShowsList.filter { TvShow ->
                val a = (moviesGenres.find { it -> it.name.toLowerCase().contains(query.toLowerCase()) })
                if(a != null) {
                    TvShow.genre_ids.contains(a.id)
                }
                else {
                    false
                }

            } as ArrayList<TvShow>

            if (newTvShow.size == TVShowsList.size && !bFirstUse ) {
                DiffUtil.calculateDiff(PhotoRowDiffCallback(newTvShow, filteredTVShows), false).dispatchUpdatesTo(this)
            }
            else {
                DiffUtil.calculateDiff(PhotoRowDiffCallback(newTvShow, TVShowsList), false).dispatchUpdatesTo(this)
                bFirstUse = false
            }

            filteredTVShows = newTvShow
        }
    }

    inner class PhotoRowDiffCallback(private val newRows : List<TvShow>, private val oldRows : List<TvShow>) : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldRow = oldRows[oldItemPosition]
            val newRow = newRows[newItemPosition]
            return oldRow == newRow
        }

        override fun getOldListSize(): Int = oldRows.size

        override fun getNewListSize(): Int = newRows.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldRow = oldRows[oldItemPosition]
            val newRow = newRows[newItemPosition]
            return oldRow == newRow
        }
    }

    override fun getItemCount() = if(bFiltering) filteredTVShows.size else TVShowsList.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val idView: ImageView = view.iv_movie
        val tvMovieName: TextView = view.tv_movie_name
    }
}