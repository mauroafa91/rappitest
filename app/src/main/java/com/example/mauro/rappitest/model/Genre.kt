package com.example.mauro.rappitest.model

import java.io.Serializable

class Genres: Serializable{

    var genres: ArrayList<Genre> = ArrayList()

    constructor()

    constructor(genres: ArrayList<Genre>) {
        this.genres = genres
    }
}

class Genre: Serializable {
    var id: Int = 0
    var name: String = ""

    constructor()

    constructor(id: Int, name: String) {
        this.id = id
        this.name = name
    }
}