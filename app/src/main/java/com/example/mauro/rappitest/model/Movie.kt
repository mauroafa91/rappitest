package com.example.mauro.rappitest.model

import java.io.Serializable

class Movie: Serializable {

    var poster_path: String = ""
    var adult: Boolean = false
    var overview: String = ""
    var release_date: String = ""
    var genre_ids: ArrayList<Int> = ArrayList()
    var id: Int = 0
    var original_title: String = ""
    var original_language: String = ""
    var title: String = ""
    var backdrop_path: String = ""
    var popularity: Double = 0.0
    var vote_count: Int = 0
    var video: Boolean = false
    var vote_average: Double = 0.0

    constructor()

    constructor(poster_path: String, adult: Boolean, overview: String, release_date: String, genre_ids: ArrayList<Int>, id: Int, original_title: String, original_language: String, title: String, backdrop_path: String, popularity: Double, vote_count: Int, video: Boolean, vote_average: Double) {
        this.poster_path = poster_path
        this.adult = adult
        this.overview = overview
        this.release_date = release_date
        this.genre_ids = genre_ids
        this.id = id
        this.original_title = original_title
        this.original_language = original_language
        this.title = title
        this.backdrop_path = backdrop_path
        this.popularity = popularity
        this.vote_count = vote_count
        this.video = video
        this.vote_average = vote_average
    }
}