# RappiTest

La aplicacion fue diseñada utilizando algunas librerias externas, Retrofit2, HttpOK y  Glide
Se desarrollo en lenguaje Kotlin

Separe en packages las clases.

Activities, que adentro tiene Common, movies y TVShows
Adapter (Contiene 5 adaptadores que se usan en el proyecto entero, una para peliculas, uno para series, y los otros 3 para visualizar los detalles) 
Interface (Contiene los llamados de Retrofit)
Model (El modelo de clases entero utilizado)
Utils (Retrofit client y Glide)

La forma en la que se desarrollo fue con un bottomNavigation menu para seleccione el tipo de busqueda el usuario desea realizar, iniciando en la seccion de peliculas.
Arriba el usuario tiene acceso a dos botones, el de busqueda que te permite buscar localmente la vista actual y otro para cambiar a la seccion de TV Shows.
Al estar en la parte de TV shows ese boton de reemplaza al de pelicula y los 3 botones de abajo cambiar a ser 2 botones debido a que no existe sección de "Upcoming"

En la seccion de peliculas se podran ver por Popular, Top Rated, Upcoming, en la seccion de series solamente se vera Popular y Top rated
Para ambas secciones:
- Se puede visualizar el detalle de cada objeto, es un detalle distinto para peliculas o series
- Se puede realizar una busqueda local por genero.
- NO se puede visualizar el video (trailer) de cada objeto
- En la seccion de series no se pueden desglozar los episodios por temporada
- No se puede realizar busqueda online en la aplicacion

Mas a fondo las clases

BaseMoviesActivity es la clase en la cual se define el layout con el bottom navigation con un frame que luego se va reemplazando dependiendo de que clase
se vaya llamando
Otras 3 clases heredan de esta (PopularMoviesActivity, TopRatedMoviesAcitivty y UpcomingMoviesActivity)
Luego tendriamos una clase para los detalles de las peliculas que se llama MoviesDetailActivity la cual tiene un fragmento llamado MoviesDetailFragment que se encarga de mostrar el detalle

Lo mismo aplicaria para las series pero eliminando el Upcoming ya que no existe en la documentacion de la API

-Codigo limpio
En mi opinion, un buen codigo limpio es aquel que puede ser legible, entendible rapidamente y puede ser modificado rapidamente o reutilizado para no tener que realizar nueva funcionalidad o rehacer el que ya existe.

-Responsaibilidad unica
Esto va ligado al concepto de codigo limpio, basicamente dice que una funcion deberia tener un solo objetivo, si tiene mas de uno se hace mas dificil de leer y de modificar.
